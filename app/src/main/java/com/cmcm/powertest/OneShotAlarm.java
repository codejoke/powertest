package com.cmcm.powertest;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by eric on 15-12-18.
 */
public class OneShotAlarm extends BroadcastReceiver {
    private static final String TAG = OneShotAlarm.class.getSimpleName();
    public static boolean sStopOneShort = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        Slog.d(TAG, "onReceive(), ENTER");

        Toast.makeText(context, R.string.one_shot_received, Toast.LENGTH_SHORT).show();
        Utils.playASound(context);

        Slog.d(TAG, "onReceive(), sStopOneShort=" + sStopOneShort);
        if (!sStopOneShort) {
            setupOneShotAlarm(context);
        }
    }

    public static void setupOneShotAlarm(final Context context) {
        Slog.d(TAG, "setupOneShotAlarm(), ENTER");

        Intent intent = new Intent(context, OneShotAlarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);

        // We want the alarm to go off 15 seconds from now.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 15);

        // Schedule the alarm!
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
    }

    public static void cancelOneShotAlarm(final Context context) {
        Slog.d(TAG, "cancelOneShotAlarm(), ENTER");

        Intent intent = new Intent(context, OneShotAlarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);

        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(sender);
    }
}
