package com.cmcm.powertest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.widget.Toast;

/**
 * Created by eric on 15-12-18.
 */

public class RepeatingAlarm extends BroadcastReceiver {
    private static final String TAG = RepeatingAlarm.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Slog.d(TAG, "onReceive(), ENTER");

        Toast.makeText(context, R.string.repeating_received, Toast.LENGTH_SHORT).show();

        playASound(context);
    }

    private void playASound(Context context) {
        MediaPlayer mp = MediaPlayer.create(context, R.raw.sedna);
        mp.start();
    }

}