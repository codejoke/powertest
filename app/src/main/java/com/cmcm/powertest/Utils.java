package com.cmcm.powertest;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by eric on 15-12-18.
 */
public class Utils {
    private static final String TAG = Utils.class.getSimpleName();

    public final static void playASound(Context context) {
        Slog.d(TAG, "Play a sound, ENTER");
        MediaPlayer mp = MediaPlayer.create(context, R.raw.sedna);
        mp.setLooping(false);
        mp.start();

        try {
            Thread.sleep(2 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (mp.isPlaying()) {
            mp.stop();
        }
    }
}
