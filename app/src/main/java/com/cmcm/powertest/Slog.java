package com.cmcm.powertest;

/**
 * Created by eric on 15-12-18.
 */

public final class Slog {
    public static final String APP_TAG = "PowerTest";
    public static final boolean DBG = BuildConfig.DEBUG;
    public static final boolean V = false;
    public static final boolean VV = false;

    private Slog() {}

    private static String formatMsg(String tag, String msg) {
        if (DBG) {
            return String.format("================> %s - %s", tag, msg);
        } else {
            return String.format("%s - %s", tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        android.util.Log.e(APP_TAG, formatMsg(tag, msg));
    }

    public static void e(String tag, String msg, Throwable tr) {
        android.util.Log.e(APP_TAG, formatMsg(tag, msg), tr);
    }

    public static void w(String tag, String msg) {
        android.util.Log.w(APP_TAG, formatMsg(tag, msg));
    }

    public static void w(String tag, String msg, Throwable tr) {
        android.util.Log.w(APP_TAG, formatMsg(tag, msg), tr);
    }

    public static void i(String tag, String msg) {
        android.util.Log.i(APP_TAG, formatMsg(tag, msg));
    }

    public static void i(String tag, String msg, Throwable tr) {
        android.util.Log.i(APP_TAG, formatMsg(tag, msg), tr);
    }

    public static void d(String tag, String msg) {
        android.util.Log.d(APP_TAG, formatMsg(tag, msg));
    }

    public static void d(String tag, String msg, Throwable tr) {
        android.util.Log.d(APP_TAG, formatMsg(tag, msg), tr);
    }

    public static void v(String tag, String msg) {
        android.util.Log.v(APP_TAG, formatMsg(tag, msg));
    }

    public static void v(String tag, String msg, Throwable tr) {
        android.util.Log.v(APP_TAG, formatMsg(tag, msg), tr);
    }
}